%EXPANSION OF A REAL-VALUED 2D SIGNAL INTO FOURIER COMPONENTS WHERE IN THE Y-DIRECTION ONLY COS-TERMS APPEAR
%Author: Marco Redolfi

%The signal fjk (K rows by J columns) is expanded in NxM components of the form:
% 	cos(2*pi*m*(k+0.5)/(2*K) )*real( Anm(m+1,n+1)*exp(2*pi*i*n*(j+0.5)/J) );

function [Anm,Enm]=FT2Dcos(fjk,varargin)

%OUTPUTS
%Anm: matrix containing complex Fourier coefficient
	%->rows represent the y-wavenumber (m); columns represent the x-wavenumber (n)
	%->the real part represent cosine components along x; the imaginary part represent sine components along x
%Enm: energy (i.e. variance) associated to each Fourier coefficient Anm

%REQUIRED INPUTS
%fjk: input matrix where rows represent the different y-values and columns represent x-values

%OPTIONAL PARAMETERS
%plot maps:  figures with the input and output maps [boolean] (default=false):

%EXAMPLES OF USAGE
%Anm=FT2Dcos(fjk)
%[Anm,Enm]=FT2Dcos(fjk)
%[Anm,Enm]=FT2Dcos(fjk,'plot_maps',true)

%*********************************************************************************************************

%Parsing optional input
p = inputParser;
addRequired(p,'fjk',@isreal);
addParameter(p,'plot_maps',false,@islogical);
parse(p,fjk,varargin{:});
plot_maps=p.Results.plot_maps;


%% 1) Calculating Fourier coefficients

%Grid size
J=size(fjk,2);
K=size(fjk,1); 

%Mirroring the signal along the y direction
%	->this allows for obtaining a Fourier representation where only cos-terms appear in the y-direction
fjk_flip=flipud(fjk);
fjk_ext=[fjk;fjk_flip]; %Extended signal

%Fast Fourier transform 
Fnm_ext=fft2(fjk_ext)/(J*2*K); %Normalized Fourier coefficients

%Shifting back the system of reference by half step (in both directions)
n=[0:J-1];
m=[0:2*K-1];
Fnm_star_ext=Fnm_ext.*( exp(-pi*i*m'/(2*K))*exp(-pi*i*n/J) );


%% From general representation in terms of Fnm coefficient to specific representation in terms of Anm coefficient
% ->this in possible because the signal is real-valued and mirrored 

%Number of transverse (N) and longitudinal (M) components needed to represent the signal
N=floor(J/2)+1;
M=K;

%Coefficient outside the first quadrant contain redundant information
Anm=Fnm_star_ext(1:M,1:N); 

%Most coefficients need to be multiplied by four, as their represent components that appears in all the four quadrants
Anm=4*Anm;

%The 1D components (n=0 and m=0) appear only twice
Anm(1,:)=Anm(1,:)/2;
Anm(:,1)=Anm(:,1)/2;

%If J is even, also the n=N-1 component appear only twice
if mod(J,2)==0 
	Anm(:,end)=Anm(:,end)/2;
end


%% 2) Calculating energy content of each harmonic

Enm=abs(Anm.^2)/4; %The energy of a 2D sinusoidal signal is 1/4 the amplitude
Enm(:,1)=Enm(:,1)*2; %The energy of a 1D sinusoidal signal (constant along x) is 1/2 the amplitude
if mod(J,2)==0 %If the number of columns is even
	Enm(:,end)=Enm(:,end)*2; %The energy of a 1D signal that varies as [+1,-1,+1,-1,...] along x equals 1/2 the amplitude
end
Enm(1,:)=Enm(1,:)*2; %The energy of a 1D sinusoidal signal (constant along y) is 1/2 the amplitude

if plot_maps

	%% Plotting input signal		
	
	j=[0:J-1];
	k=[0:2*K-1];

	figure('Name','Extended input signal')
		imagesc([0,J-1],[0:2*K-1],real(fjk_ext))
		xlabel('j [-]')
		ylabel('k [-]')
		set(gca,'Ydir','normal')
		colorbar


	%% Plotting Fourier FFT2 coefficient

	n=[0:N-1];
	m=[0:M-1];

	figure('Name','Fourier Fnm coefficients')
		subplot(2,1,1)
		imagesc([0,N],[0,M],real(Fnm_star_ext))
		title('Real part Re(Fnm^*)')
		xlabel('n [-]')
		ylabel('m [-]')
		set(gca,'Ydir','normal')
		colorbar

		subplot(2,1,2)
		imagesc([0,N],[0,M],imag(Fnm_star_ext))
		title('Imaginary part Im(Fnm^*)')
		xlabel('n [-]')
		ylabel('m [-]')
		set(gca,'Ydir','normal')
		colorbar

end


