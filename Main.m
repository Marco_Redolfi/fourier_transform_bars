% EXAMPLE: APPLICATION OF THE MATLAB FUNCTION FT2Dcos TO ANALYZE DIFFERENT TEST CASES
%Author: Marco Redolfi

close all
clear all
clc

%% Input parameters

%Select test case. Available options: 'pure_sinusoid'; 'random_1D'; 'random_2D'; 'random_2D_alternate'
test_signal='pure_sinusoid'

L=10; %Domain length
W=2;  %Domain width
J=10; %Number of grid points along the x-direction
K=6;  %Number of grid points along the y-direction


%% Building regular grid

%Indexes of the grid points
j=[0:J-1]; %Index in the x-direction
k=[0:K-1]; %Index in the y-direction

%Grid spacing
dx=L/J; %x-spacing
dy=W/K; %y-spacing

%Coordinates of the grid points
x=(j+1/2)*dx;
y=(k+1/2)*dy;

%Matrices of grid coordinates
[X,Y]=meshgrid(x,y);


%% Defining signal fjk

switch test_signal

case 'pure_sinusoid'

	%Double (in x and y) sinusoid representing the fundamental harmonic
	A=1; %Amplitude
	phi=0; %Phase shift
	fjk=A*cos(2*pi*X/L+phi).*cos(pi*Y/W);

case 'random_1D'

	%Purely longitudinal random variation uniformly distributed between -1 and +1
	fjk=2*repmat(rand(1,J),K,1)-1;

case 'random_2D'
	
	%Two-dimensional random variation uniformly distributed between -1 and +1
	fjk=2*rand(K,J)-1;

case 'random_2D_alternate'

	%Random 2-D signal with purely alternate pattern
	if mod(J,2)~=0
		error('An even number J is needed to reproduce the alternate pattern')
	end
	fjk=2*rand(K,J/2)-1; %first part of the map
	fjk=[fjk,flipud(fjk)]; %the second part is obtained by mirroring the first part along the x-axis

end


%% Fourier analysis through the FT2Dcos function

Anm=FT2Dcos(fjk,'plot_maps',false);


%% Plotting signal fjk

figure('Name','Signal')

	imagesc(x,y,fjk)
	xlabel('x')
	ylabel('y')
	set(gca,'Ydir','normal')
	colorbar


%% Plotting Fourier Anm coefficients

N=size(Anm,2); %Number of Fourier modes in the x-direction
M=size(Anm,1); %Number of Fourier modes in the y-direction
n=[0:N-1]; %Array of longitudinal modes
m=[0:M-1]; %Array of transverse modes

%Plot limits
z_min=min(min([real(Anm),imag(Anm)]));
z_max=max(max([real(Anm),imag(Anm)]));

%1) Real and imaginary part
figure('Name','Fourier Anm coefficients')
	
subplot(2,1,1)
	imagesc([0,N-1],[0,M],real(Anm))
	title('Real part Re(Anm)')
	xlabel('n [-]')
	ylabel('m [-]')
	set(gca,'Ydir','normal')
	caxis([z_min,z_max])
	colorbar

subplot(2,1,2)
	imagesc([0,N-1],[0,M],imag(Anm))
	title('Imaginary part Im(Anm)')
	xlabel('n [-]')
	ylabel('m [-]')
	set(gca,'Ydir','normal')
	caxis([z_min,z_max])
	colorbar

%2) Absolute value
figure('Name','Fourier Anm coefficients (absolute value)')

	imagesc([0,N-1],[0,M],abs(Anm))
	title('Absolute value |Anm|')
	xlabel('n [-]')
	ylabel('m [-]')
	set(gca,'Ydir','normal')
	colorbar
